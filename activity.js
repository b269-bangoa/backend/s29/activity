// Insert Documents
db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
	    {
			firstName: "John",
			lastName: "Smith",
			age: 50,
			contact: {
				phone: "12435678",
				email: "jsmith@gmail.com"
			},
			courses: ["CSS", "HTML"],
			department: "HR",
			status: "active"
		},
		    {
				firstName: "Juan",
				lastName: "Tamad",
				age: 35,
				contact: {
					phone: "12345876",
					email: "tamadj@gmail.com"
				},
				courses: ["HTML", "Laravel", "Sass"],
				department: "Operations",
				status: "active"
			}
]);


db.users.find(
	{$or: [{ firstName: {$regex: 'N', $options: '$i'}}, {lastName: {$regex: 'D', $options: '$i'}}]
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);


db.users.find(
	{$and: [{ department: "HR"}, {age: {$gte: 70}}]}
);


db.users.find(
	{$and: [{ firstName: {$regex: 'E', $options: '$i'}}, {age: {$lte: 30}}]
	}
);


